#!/usr/bin/env python
# -*- coding: utf8 -*-

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#


import sys, re, argparse, requests, json, urlparse

parser = argparse.ArgumentParser(description = 'Importe les freeboxtv dans tvheadend 4.0')

parser.add_argument('--hts-url', default = 'http://localhost:9981/', help = 'url vers le serveur hts (http://localhost:9981 par défaut)')
parser.add_argument('--hts-user', default = 'admin', help = 'nom d\'utilisateur hts (admin par défaut)')
parser.add_argument('--hts-password', default = 'admin', help = 'mot de passe hts (admin par défaut)')

parser_grp_create = parser.add_argument_group('Création d\'un network')
parser_grp_create.add_argument('--hts-network-create', action = 'store_true')
parser_grp_create.add_argument('--hts-network-name', default = 'FreeBoxTV', help = 'Nom du netwoark à créer (FreeBoxTV par défaut)')

parser_grp_list = parser.add_argument_group('Liste des networks')
parser_grp_list.add_argument('--hts-networks-list', action = 'store_true')

parser_grp_m3uimport = parser.add_argument_group('Import des chaines')
parser_grp_m3uimport.add_argument('--hts-network-uuid', default = None, help = 'uuid du network (voir --hts-networks-list ou fake pour afficher les actions sans les executés)')
parser_grp_m3uimport.add_argument('--playlist-url', default = 'http://mafreebox.freebox.fr/freeboxtv/playlist.m3u', help = 'url vers la playlist (http://mafreebox.freebox.fr/freeboxtv/playlist.m3u par défaut)')
parser_grp_m3uimport.add_argument('--prefered_quality', default = 'auto,hd,sd,ld', help = 'Ordre de préférence des qualités de flux (auto,hd,sd,ld)')

args = parser.parse_args()

if args.hts_network_create :
    '''
        Créer un "network"
    '''
    r = requests.post(
        args.hts_url+'/api/mpegts/network/create',
        auth=(args.hts_user, args.hts_password),
        data={
            "class":"iptv_network",
            "conf":json.dumps({
                "networkname":args.hts_network_name,
                "autodiscovery":True,
                "skipinitscan":True,
                "sid_chnum":False,
                "ignore_chnum":False,
                "satip_source":0,
                "max_streams":2,
                "max_bandwidth":0,
                "max_timeout":15,
                "nid":0,
                "idlescan":False,
                "charset":"",
                "localtime":False,
                "priority":1,
                "spriority":1
              })
          }
      )
    if r.ok :
        print "Crée"
        sys.exit(0)
    else :
        print "Erreur http %i (%s)"%(r.status_code, r.url)
        sys.exit(1)

if args.hts_networks_list :
    '''
        Lister les "networks"
    '''
    r = requests.get(args.hts_url+'api/mpegts/network/grid',auth=(args.hts_user, args.hts_password))
    if r.ok :
        for n in r.json()['entries'] :
            if n.has_key("networkname") :
                print "%(uuid)s (%(networkname)s)"%n
            else :
                print "%(uuid)s (N/A)"%n
        sys.exit(0)
    else :
        print "Erreur http %i (%s)"%(r.status_code, r.url)
        sys.exit(1)

if args.hts_network_uuid :
    '''
        Import m3u
    '''
    def is_prefered_quality () :
        if not m3u.has_key(params.get('service', params.get('frontend'))[0]) :
            '''
                Le flux ne se trouve pas encore dans le m3u filtré. On l'ajoute
            '''
            return True
        if not m3u[params.get('service', params.get('frontend'))[0]]['quality'] in prefered_quality :
            '''
                Le flux dans le m3u filtrer n'a pas un qualité citée dans les
                préférences. On le remplace.
            '''
            return True
        if not params['flavour'][0] in prefered_quality :
            '''
                Le flux n'a pas un qualité citée dans les préférances, on ne
                remplace pas celui dans le m3u filtrer
            '''
            return False
        if prefered_quality.index(params['flavour'][0]) < prefered_quality.index(m3u[params.get('service', params.get('frontend'))[0]]['quality']) :
            '''
                On conserve le flux préféré
            '''
            return True

    prefered_quality = args.prefered_quality.split(',')
    pattern = re.compile("^#EXTINF:0,([0-9]+) - (.*?)(?: HD$| \(.*\)$|$)")
    m3u = {}
    r = requests.get(args.playlist_url)
    if r.ok :
        for l in r.iter_lines() :
            print l
            if l.startswith('#EXTINF'):
                match = pattern.search(l)
            elif l.startswith('rtsp'):
                params = urlparse.parse_qs(urlparse.urlparse(l).query)
		print params
                if not 'flavour' in params :
                    params['flavour'] = ['auto',]
                if is_prefered_quality() :
                    print   "channel found\n" + \
                            " num: " + match.group(1) + \
                            " name: " + match.group(2)
                    m3u[params.get('service', params.get('frontend'))[0]] = {
                        'num': match.group(1),
                        'name': match.group(2),
                        'quality': params['flavour'][0],
                        'url': l
                      }
    else :
        print "Erreur %i, le téléchargement de la playlist a échoué (%s)"%(r.status_code, r.url)
        sys.exit(1)

    for ch in m3u.values() :
        print 'Création du mux %(num)s - %(name)s (url : %(url)s, qualité %(quality)s)'%ch
        if args.hts_network_uuid != 'fake':
            r = requests.post(
                args.hts_url+'/api/mpegts/network/mux_create',
                auth=(args.hts_user, args.hts_password),
                data={
                    "uuid":args.hts_network_uuid,
                    "conf":json.dumps({
                        "enabled":True,
                        "epg":1,
                        "scan_state":0,
                        "pmt_06_ac3":0,
                        "channel_number":ch['num'],
                        "iptv_url":ch['url'],
                        "iptv_interface":"",
                        "iptv_atsc":False,
                        "iptv_muxname":ch['name'],
                        "iptv_sname":ch['name'],
                        "charset":"",
                        "priority":0,
                        "spriority":0,
                        "iptv_respawn":True,
                        "iptv_env":""
                      })
                  }
              )
            if not r.ok :
                print "Attention, la création du mux a échoué"

    if args.hts_network_uuid == 'fake' :
        print "Mode fake aucune modification n'a été réalisée"
